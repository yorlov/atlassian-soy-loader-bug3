import {greeting} from './app.soy';

const element = document.getElementById('app');

element.innerHTML = greeting({content: 'Hello'});